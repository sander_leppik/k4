import java.util.Objects;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   private long numer;
   private long denom;

   /** Main method. Different tests. */
   public static void main (String[] param) {
       Lfraction a = new Lfraction(3, 4);
       Lfraction b = new Lfraction(4, 5);
      System.out.println(a.equals(b));
   }

   // TODO!!! instance variables here

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if(b==0){
         throw new RuntimeException("Denominator can't be 0");
      }

      long commonDivisor = getSmallestCommonDivisor(a, b);
      if((commonDivisor < 0 && b >= 0) || (commonDivisor >= 0 && b < 0)){
         a = -a;
         b = -b;
      }

      numer = a/commonDivisor;
      denom = b/commonDivisor;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {

      return numer;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 

      return denom;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {

      return Long.toString(numer) + "/" + Long.toString(denom);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if(m instanceof Lfraction){
         Lfraction other = (Lfraction) m;
         if(compareTo(other) == 0){
            return true;
         }
//         return (numer/other.numer) == (denom/other.denom);
      }
      return false;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      Long commonDivisor =getSmallestCommonDivisor(numer, denom);
      return Objects.hash(numer/commonDivisor, denom/commonDivisor);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long newDenom = denom * m.denom;
      long newNumer = (numer*m.denom) + (m.numer*denom);
      long commondivisor = getSmallestCommonDivisor(newNumer, newDenom);
      return new Lfraction(newNumer/commondivisor, newDenom/commondivisor);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long newNumer = numer * m.numer;
      long newDenom = denom * m.denom;
      long commonDivisor = getSmallestCommonDivisor(newNumer, newDenom);

      return new Lfraction(newNumer/commonDivisor, newDenom/commonDivisor);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if(numer == 0){
         throw new RuntimeException("Can not inverse, numerator is 0");
      }
      if(numer < 0){
         return new Lfraction(-denom, -numer);
      }
      return new Lfraction(denom, numer);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numer, denom);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
//      Long newDenom = denom*m.denom;
//      Long newNumer = (numer*m.denom - m.numer*denom);
//      Long commonDivisor = getSmallestCommonDivisor(newNumer, newDenom);
//
//      return new Lfraction(newNumer/commonDivisor, newDenom/commonDivisor);
      return plus(new Lfraction(-m.numer, m.denom));
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
//      long newNumer = numer*m.denom;
//      long newDenom = denom*m.numer;
//
//      long commonDivisor = getSmallestCommonDivisor(newNumer, newDenom);
//
//      return new Lfraction(newNumer/commonDivisor, newDenom/commonDivisor);
       if(m.numer == 0){
           throw new RuntimeException("Cannot divide, numerator is 0 in: " + m.toString());
       }

      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if(numer*m.denom < m.numer*denom){
         return -1;
      }else if(numer*m.denom > m.numer*denom) {
         return 1;
      }else if(numer*m.denom == m.numer*denom){
         return 0;
      }
      return 0;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numer, denom);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return Math.floorDiv(numer, denom);
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return this.minus(new Lfraction(integerPart(), 1));
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      double newNumer = numer;
      double newDenom = denom;
      return newNumer/newDenom;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long newNumer = Math.round(f*d);

      return new Lfraction(newNumer, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if(!isNumberic(s.substring(s.length() - 1))){
         throw new RuntimeException("Not a correct fraction: " + s);
      }
      String[] strArr = s.split("/");
      if(strArr.length != 2){
         throw new RuntimeException("Not a correct fraction: " + s);
      }
      if (!isNumberic(strArr[1])){
         throw new RuntimeException("Denominator not a number in: " + s);
      }
      if (!isNumberic(strArr[1])){
         throw new RuntimeException("Numerator not a number in: " + s);
      }

      return new Lfraction(Integer.valueOf(strArr[0]), Integer.valueOf(strArr[1]));
   }


   //https://www.baeldung.com/java-greatest-common-divisor
   private static Long getSmallestCommonDivisor(long a, long b){

      if(b == 0){
         return a;
      }
      return getSmallestCommonDivisor(b, a % b);
   }

   private static boolean isNumberic(String s){
      try{
         int num = Integer.parseInt(s);
      }catch(NumberFormatException e){
         return false;
      }
      return true;
   }
}

